import sys
import re
import os
from os import listdir
from os.path import isfile
from os import access

# execute in the api/samples folder

rel_wsdl_path = "../api/"
java_path = "java_axis2/"
php_path = "php/"

java_name_re = ".+\.setName\(\"(.+)\"\);"
java_version_re = ".+\.setVersion\((.+)\);"

php_api_re = ".+[ne]\([\"\'](.+?)[\"\'], [\"\'](.+?)[\"\'], (.+?),.+" 

wsdls = [f for f in listdir(rel_wsdl_path) if isfile(rel_wsdl_path+f) and f.endswith('wsdl')]
javas = [f for f in listdir(java_path) if isfile(java_path+f) and f.endswith("java")]
phps = [f for f in listdir(php_path) if isfile(php_path+f) and access(php_path+f, os.X_OK)]

def wsdl_exists(wsdl_file_name):
	wsdl_path = rel_wsdl_path + wsdl_file_name + ".wsdl"
	if isfile(wsdl_path):
		print("\t" + wsdl_file_name + " exists, good")
		return True
	else:
		print("\t" + wsdl_file_name + " does not exists, BAD <------")
		return False

def check_java_samples():
	# checking all java samples
	for f in javas:
		wsdl = open(java_path+f, "r")
		print("checking java sample " + f + ":")

		api_name = ""
		api_version = ""
		for line in wsdl:
			if re.search(java_name_re, line):
				api_name = re.search(java_name_re, line).group(1)
			if re.search(java_version_re, line):
				api_version = re.search(java_version_re, line).group(1)
			if(api_name and api_version):
				#print (api_name + "_v" + api_version)
				wsdl_exists(api_name+"_v"+api_version)	
				api_name=""
				api_version=""

def check_php_samples():
	# checking all php samples
	for f in phps:
		wsdl = open(php_path+f, "r")
		print("checking php sample " + f + ":")
		
		for line in wsdl:
			if re.search(php_api_re, line):
				api_name=re.search(php_api_re, line).group(1)+"_mgr_"+re.search(php_api_re, line).group(2)
				api_version=re.search(php_api_re, line).group(3)
				wsdl_exists(api_name+"_v"+api_version)	

check_java_samples()
print("------------------------------------------------------------")
check_php_samples()

