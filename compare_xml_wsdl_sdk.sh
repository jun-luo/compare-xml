#!/bin/bash

#
# This script compares wsdl files across three platforms:
#    - Ubuntu 64bit, Ubuntu 32bit, Windows
#
# All wsdl files should be placed in:
#    - Ubuntu 64bit: ./u64 folder
#    - Windows:      ./w32 folder
#    - Ubuntu 32bit: ./u32 folder
#
# The script will exit with 0 if all files are identical, otherwise 1.
#
# NOTE: the script does not verify that a wsdl file exists within all three
# folders, this need to be done prior to running this script

WIN32='w32'
U64='u64'
U32='u32'

if [[ $1 != 'doit' ]]; then
        echo 'I refuse to do it unless you read the source code.'
        exit 2
fi

EXIT_CODE=0

for XML_TO_COMPARE in $( ls $WIN32); do

        xmllint --format --c14n $WIN32/$XML_TO_COMPARE > $XML_TO_COMPARE.$WIN32.XML
        xmllint --format --c14n $U64/$XML_TO_COMPARE > $XML_TO_COMPARE.$U64.XML
        xmllint --format --c14n $U32/$XML_TO_COMPARE > $XML_TO_COMPARE.$U32.XML

        diff $XML_TO_COMPARE.$WIN32.XML $XML_TO_COMPARE.$U64.XML 1>/dev/null
        DIFF_R1=`echo $?`
        diff $XML_TO_COMPARE.$U32.XML $XML_TO_COMPARE.$U64.XML 1>/dev/null
        DIFF_R2=`echo $?`

        if [[ DIFF_R1 -eq 0 && DIFF_R2 -eq 0 ]]; then
                echo "identical: $XML_TO_COMPARE"
        else
                echo "FILE $XML_TO_COMPARE differs across platforms"
                EXIT_CODE=1
        fi

        rm $XML_TO_COMPARE.$WIN32.XML $XML_TO_COMPARE.$U64.XML $XML_TO_COMPARE.$U32.XML

done

exit $EXIT_CODE
